import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import( '../views/LoginView.vue'), //登入页面
  },

  {
    path: '/home',
    component: () => import( '../views/HomeView.vue'), //后台主体框架
    redirect: '/system/userinfo',
    children: [
        {
        path: '/system/userinfo',
        name: 'userinfo',
        component: () => import( '../views/system/UserInfo.vue'), //个人信息
      },{
        path: '/business/users/changePwd',
        name: 'changePwd',
        component: () => import( '../views/system/ChangePwd.vue'), //修改密码
      },
      {
        path: '/business/users',
        name: 'users',
        component: () => import( '../views/system/UserView.vue'), //用户管理
      },
      {
        path: '/business/department',
        name: 'departments',
        component: () => import( '../views/system/DeptView.vue'), //部门管理
      },
      {
        path: '/business/role',
        name: 'role',
        component: () => import( '../views/system/RoleView.vue'), //角色管理
      },
      {
        path: '/business/menu',
        name: 'menu',
        component: () => import( '../views/system/MenuView.vue'), //菜单管理
      },


      ]
  },
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

import store from '../store'//引入store
import LocalStorage from "../assets/js/local-storage";


//路由导航守卫
router.beforeEach((to, from, next) => {

  const token = LocalStorage.get('PERMISSION_ACCESS_TOKEN');
  if (to.path === '/login') {
    if (!token) {
      return next();
    } else {
      return next({path: '/home'})
    }
  }

  if (to.path === '/error/ErrorPage') {
    return next();
  }

  if (!token) {
    return next('/login');
  } else {
    //判断是否有访问该路径的权限
    const urls = store.state.userInfo.urls;
    //如果是管理员
    if (store.state.userInfo.isAdmin) {
      return next();
    } else {
      if (urls.indexOf(to.path) > -1) {
        //则包含该元素
        window.sessionStorage.setItem("activePath", to.path);
        return next();
      } else {
        return next("/error/ErrorPage");
      }
    }
  }
})

export default router
