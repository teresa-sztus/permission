package com.sztus.permission.shiro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


//@EnableDiscoveryClient
//@EnableFeignClients(basePackages = "com.sztus")
@SpringBootApplication(scanBasePackages ="com.sztus")
public class ShiroApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiroApplication.class, args);
    }

}
