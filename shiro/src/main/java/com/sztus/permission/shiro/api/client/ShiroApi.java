package com.sztus.permission.shiro.api.client;

import com.sztus.permission.shiro.object.domain.User;
import com.sztus.permission.shiro.object.type.response.ResponseBean;
import org.springframework.web.bind.annotation.GetMapping;

//@FeignClient(value = "permission-shiro", contextId = "permission-business")
public interface ShiroApi {

    @GetMapping("/info")
    ResponseBean<User> findUser();

}
