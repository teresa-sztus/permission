package com.sztus.permission.shiro.config;

import com.sztus.permission.shiro.object.domain.Menu;
import com.sztus.permission.shiro.object.domain.Role;
import com.sztus.permission.shiro.object.domain.User;
import com.sztus.permission.shiro.object.type.response.ActiveUser;
import com.sztus.permission.shiro.service.LoginService;
import com.sztus.permission.shiro.util.JWTUtils;
import lombok.SneakyThrows;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
public class UserRealm extends AuthorizingRealm {

    @Autowired
    private LoginService loginService;

    /**
     * 必须重写此方法，不然Shiro会报错
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JWTToken;
    }

    /**
     *
     * 验证用户权限
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        ActiveUser activeUser = (ActiveUser) SecurityUtils.getSubject().getPrincipal();

        //type=0为超级管理员，拥有所有权限
        if(activeUser.getUser().getType()==0){
            authorizationInfo.addStringPermission("*:*");
        }else {
            List<String> marks = activeUser.getMarks();
            List<Role> roleList = activeUser.getRoles();
            //授权角色
            if (!CollectionUtils.isEmpty(roleList)) {
                for (Role role : roleList) {
                    authorizationInfo.addRole(role.getRoleName());
                }
            }
            //授权权限
            if (!CollectionUtils.isEmpty(marks)) {
                for (String  mark : marks) {
                    if (mark != null && !"".equals(mark)) {
                        authorizationInfo.addStringPermission(mark);
                    }
                }
            }
        }
        return authorizationInfo;
    }

    /**
     * 进行账号验证
     */
    @SneakyThrows
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken auth) throws AuthenticationException {
        String token = (String) auth.getCredentials();
        // 解密获得username，用于和数据库进行对比
        String username = JWTUtils.getUsername(token);

        if (Objects.isNull(username)) {
            throw new AuthenticationException(" token错误，请重新登入！");
        }

        User user = loginService.findUserByName(username);

        if (Objects.isNull(user)) {
            throw new AccountException("账号不存在!");
        }
        if(JWTUtils.isExpire(token)){
            throw new AuthenticationException(" token过期，请重新登入！");
        }

        if (! JWTUtils.verify(token, username, user.getPassword())) {
            throw new CredentialsException("密码错误!");
        }


        //如果验证通过，获取用户的角色
        List<Role> roles= loginService.findRolesById(user.getId());

        //查询该用户有用的所有菜单包括按钮
        List<Menu> menus = loginService.findMenuByRole(roles);

        Set<Long> id = new HashSet<>();
        for (Menu menu : menus){
            id.add(menu.getId());
        }
        List<String> urls = loginService.findUrlById(id);
        List<String> marks = loginService.findMarkById(id);

        ActiveUser activeUser = new ActiveUser();
        activeUser.setUser(user);
        activeUser.setRoles(roles);
        activeUser.setMenus(menus);
        activeUser.setUrls(urls);
        activeUser.setMarks(marks);

        return new SimpleAuthenticationInfo(activeUser, token, getName());

    }


}
