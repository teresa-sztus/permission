package com.sztus.permission.shiro.controller;

import com.sztus.permission.shiro.component.handler.SystemException;
import com.sztus.permission.shiro.object.type.response.ResponseBean;
import com.sztus.permission.shiro.object.type.view.DepartmentView;
import com.sztus.permission.shiro.object.type.view.PageView;
import com.sztus.permission.shiro.service.DepartmentService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

//@CrossOrigin
@RequestMapping("/business/department")
@RestController
public class DepartmentController {

    private final DepartmentService departmentService;

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    /**
     * 部门列表
     * 根据部门名称进行查询
     * @return
     */
    @GetMapping("/findDepartmentList")
    public ResponseBean<PageView<DepartmentView>> findDepartmentList(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                           @RequestParam(value = "pageSize") Integer pageSize,
                                                           DepartmentView departmentView) {
        PageView<DepartmentView> departmentList = departmentService.findDepartmentList(pageNum, pageSize, departmentView);
        return ResponseBean.success(departmentList);
    }

    @PostMapping("/add")
    public ResponseBean add(@RequestBody DepartmentView departmentView) throws SystemException {
        departmentService.add(departmentView);
        return ResponseBean.success();
    }
    @RequiresPermissions({"department:update"})
    @PutMapping("/update/{id}")
    public ResponseBean update(@PathVariable Long id,@RequestBody DepartmentView departmentView) throws SystemException, ParseException {
        int update = departmentService.update(id, departmentView);
        if (update >= 1){
            return ResponseBean.success();
        }
        return ResponseBean.error("更新部门失败");
    }

    /**
     * 编辑部门
     * @param id
     * @return
     */
    @RequiresPermissions({"department:edit"})
    @GetMapping("/edit/{id}")
    public ResponseBean edit(@PathVariable Long id) throws SystemException {
        DepartmentView departmentView = departmentService.edit(id);
        return ResponseBean.success(departmentView);
    }
    @RequiresPermissions({"department:delete"})
    @DeleteMapping("/delete/{id}")
    public ResponseBean delete(@PathVariable Long id) throws SystemException {
        departmentService.delete(id);
        return ResponseBean.success();
    }


    /**
     * 所有部门
     *
     * @return
     */
    @GetMapping("/findAll")
    public ResponseBean<List<DepartmentView>> findAll() {
        List<DepartmentView> departmentViewList = departmentService.findAll();
        return ResponseBean.success(departmentViewList);
    }

}
