package com.sztus.permission.shiro.controller;

import com.sztus.permission.shiro.component.handler.SystemException;
import com.sztus.permission.shiro.object.enumerate.CommonErrorCode;
import com.sztus.permission.shiro.object.type.request.UserLoginRequest;
import com.sztus.permission.shiro.object.type.response.ResponseBean;
import com.sztus.permission.shiro.object.type.view.MenuNodeView;
import com.sztus.permission.shiro.object.type.view.UserInfoView;
import com.sztus.permission.shiro.service.LoginService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

//@CrossOrigin(methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping("/system")
@RestController
public class LoginController {

    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    /**
     * 登录
     */
    @PostMapping("/login")
    public ResponseBean<String> login(@RequestBody UserLoginRequest loginRequest,HttpServletRequest request) throws SystemException {
        String token= loginService.login(loginRequest.getUsername(),loginRequest.getPassword());
        return ResponseBean.success(token);
    }

    /**
     * 用户信息
     *
     */
    @GetMapping("/userinfo")
    public ResponseBean<UserInfoView> info() throws SystemException {
        UserInfoView userInfoView= loginService.getUserInfo();
        if (Objects.isNull(userInfoView)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"获取用户信息失败");
        }
        return ResponseBean.success(userInfoView);
    }



    /**
     * 加载菜单树
     */
    @GetMapping("/findMenu")
    public ResponseBean<List<MenuNodeView>> findMenu() throws SystemException {
        List<MenuNodeView> menuTree = loginService.findMenu();
        if(Objects.isNull(menuTree)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"菜单加载失败");
        }
        return ResponseBean.success(menuTree);
    }


}
