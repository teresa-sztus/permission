package com.sztus.permission.shiro.controller;


import com.sztus.permission.shiro.component.handler.SystemException;
import com.sztus.permission.shiro.object.domain.Menu;
import com.sztus.permission.shiro.object.type.response.ResponseBean;
import com.sztus.permission.shiro.object.type.view.MenuNodeView;
import com.sztus.permission.shiro.object.type.view.MenuView;
import com.sztus.permission.shiro.service.MenuService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

//@CrossOrigin
@RequestMapping("/business/menu")
@RestController
public class MenuController {

    private final MenuService menuService;

    public MenuController(MenuService menuService) {
        this.menuService = menuService;
    }

    /**
     * 加载菜单树
     *
     * @return
     */
    @GetMapping("/tree")
    public ResponseBean<Map<String, Object>> tree() {
        List<MenuNodeView> menuTree = menuService.findMenuTree();
        List<Long> ids = menuService.findOpenIds();
        Map<String, Object> map = new HashMap<>();
        map.put("tree", menuTree);
        map.put("open", ids);
        return ResponseBean.success(map);
    }

    /**
     * 新增菜单/按钮
     *
     * @return
     */
    @RequiresPermissions({"menu:add"})
    @PostMapping("/add")
    public ResponseBean<Map<String, Object>> add(@RequestBody MenuView menuView) throws SystemException {
        Menu node = menuService.add(menuView);
        Map<String, Object> map = new HashMap<>();
        map.put("id", node.getId());
        map.put("menuName", node.getMenuName());
        map.put("children", new ArrayList<>());
        map.put("icon", node.getIcon());
        return ResponseBean.success(map);
    }

    /**
     * 删除菜单/按钮
     *
     * @param id
     * @return
     */
    @RequiresPermissions({"menu:delete"})
    @DeleteMapping("/delete/{id}")
    public ResponseBean deleteMenuById(@PathVariable Long id) throws SystemException {
        menuService.deleteMenuById(id);
        return ResponseBean.success();
    }

    /**
     * 菜单详情
     *
     * @param id
     * @return
     */
    @RequiresPermissions({"menu:edit"})
    @GetMapping("/edit/{id}")
    public ResponseBean<MenuView> edit(@PathVariable Long id) {
        MenuView menuView = menuService.edit(id);
        return  ResponseBean.success(menuView);
    }

    /**
     * 更新菜单
     *
     * @param id
     * @param menuView
     * @return
     */
    @RequiresPermissions({"menu:update"})
    @PutMapping("/update/{id}")
    public ResponseBean update(@PathVariable Long id, @RequestBody @Validated MenuView menuView) throws SystemException {
        menuService.update(id,menuView);
        return ResponseBean.success();
    }

}
