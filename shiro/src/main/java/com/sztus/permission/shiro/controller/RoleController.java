package com.sztus.permission.shiro.controller;

import com.sztus.permission.shiro.component.handler.SystemException;
import com.sztus.permission.shiro.object.type.response.ResponseBean;
import com.sztus.permission.shiro.object.type.view.MenuNodeView;
import com.sztus.permission.shiro.object.type.view.PageView;
import com.sztus.permission.shiro.object.type.view.RoleView;
import com.sztus.permission.shiro.service.MenuService;
import com.sztus.permission.shiro.service.RoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@CrossOrigin
@RequestMapping("/business/role")
@RestController
public class RoleController {

    private final RoleService roleService;

    private final MenuService menuService;

    public RoleController(RoleService roleService, MenuService menuService) {
        this.roleService = roleService;
        this.menuService = menuService;
    }


    /**
     * 角色授权
     *
     * @param mids
     * @return
     */
    @RequiresPermissions({"role:authority"})
    @PostMapping("/authority/{id}")
    public ResponseBean authority(@PathVariable Long id, @RequestBody Long[] mids) throws SystemException {
        roleService.authority(id,mids);
        return ResponseBean.success();
    }

    /**
     * 角色拥有的菜单权限id和菜单树
     *
     * @param id
     * @return
     */
    @GetMapping("/findRoleMenu/{id}")
    public ResponseBean<Map<String, Object>> findRoleMenu(@PathVariable Long id) {
        List<MenuNodeView> menuTree = menuService.findMenuTree();
        //该角色拥有的菜单id
        List<Long> mids = roleService.findMenuIdByRoleId(id);
        List<Long> openIds = menuService.findOpenIds();
        Map<String,Object> map = new HashMap<>();
        map.put("tree",menuTree);
        map.put("mids",mids);
        map.put("open",openIds);
        return ResponseBean.success(map);
    }

    /**
     * 角色列表
     *
     * @return
     */
    @GetMapping("/findRoleList")
    public ResponseBean<PageView<RoleView>> findRoleList(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                         @RequestParam(value = "pageSize", defaultValue = "7") Integer pageSize,
                                                         RoleView roleView) throws SystemException {
        PageView<RoleView> roleList = roleService.findRoleList(pageNum, pageSize, roleView);
        return ResponseBean.success(roleList);
    }

    /**
     * 添加角色信息
     *
     * @param roleView
     * @return
     */
    @RequiresPermissions({"role:add"})
    @PostMapping("/add")
    public ResponseBean add(@RequestBody  RoleView roleView) throws SystemException {
        roleService.add(roleView);
        return ResponseBean.success();
    }

    /**
     * 删除角色
     *
     * @param id 角色ID
     * @return
     */
    @RequiresPermissions({"role:delete"})
    @DeleteMapping("/delete/{id}")
    public ResponseBean delete(@PathVariable Long id) throws SystemException {
        roleService.delete(id);
        return ResponseBean.success();
    }


    /**
     * 编辑角色
     * 获取角色信息
     * @param id
     * @return
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions({"role:edit"})
    public ResponseBean<RoleView> edit(@PathVariable Long id) throws SystemException {
        RoleView roleView = roleService.edit(id);
        return ResponseBean.success(roleView);
    }

    /**
     * 更新角色
     *
     * @param id
     * @param roleView
     * @return
     */
    @RequiresPermissions({"role:update"})
    @PutMapping("/update/{id}")
    public ResponseBean update(@PathVariable Long id, @RequestBody RoleView roleView) throws SystemException {
        int update = roleService.update(id, roleView);
        if (update >= 1){
            return ResponseBean.success();
        }
        return ResponseBean.error("修改角色信息失败");
    }

}
