package com.sztus.permission.shiro.controller;

import com.sztus.permission.shiro.component.handler.SystemException;
import com.sztus.permission.shiro.object.type.response.ResponseBean;
import com.sztus.permission.shiro.object.type.view.PageView;
import com.sztus.permission.shiro.object.type.view.RoleTransferItemView;
import com.sztus.permission.shiro.object.type.view.UserEditView;
import com.sztus.permission.shiro.object.type.view.UserView;
import com.sztus.permission.shiro.service.RoleService;
import com.sztus.permission.shiro.service.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@CrossOrigin
@RequestMapping("/business/users")
@RestController
public class UserController {

    private final UserService userService;

    private final RoleService roleService;

    public UserController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    /**
     * 用户列表
     * 模糊查询用户列表
     * @return
     */
    @GetMapping("/findUserList")
    public ResponseBean<PageView<UserView>> findUserList(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                       @RequestParam(value = "pageSize", defaultValue = "7") Integer pageSize,
                                                         UserView userView) throws SystemException {
        List<UserView> userList = userService.findUserList(pageNum, pageSize, userView);
        return ResponseBean.success(new PageView<>(userList.size(),userList));
    }



    /**
     * 将角色分配给用户
     *
     * @param id
     * @param rids
     * @return
     */
    @RequiresPermissions({"user:assign"})
    @PostMapping("/{id}/assignRoles")
    public ResponseBean assignRoles(@PathVariable Long id, @RequestBody Long[] rids) throws SystemException {
        userService.assignRoles(id, rids);
        return ResponseBean.success();
    }

    /**
     * 根据用户ID删除用户
     *
     * @param id 用户ID
     * @return
     */
    @RequiresPermissions({"user:delete"})
    @DeleteMapping("/delete/{id}")
    public ResponseBean delete(@PathVariable Long id) throws SystemException {
        userService.deleteById(id);
        return ResponseBean.success();
    }

    /**
     * 更新用户
     *
     * @param id
     * @param userEditView
     * @return
     */
    @RequiresPermissions({"user:update"})
    @PutMapping("/update/{id}")
    public ResponseBean update(@PathVariable Long id, @RequestBody UserEditView userEditView) throws SystemException {
        userService.update(id, userEditView);
        return ResponseBean.success();
    }

    /**
     * 获取用户的详情，编辑用户信息
     * @param id
     * @return
     */
    @RequiresPermissions({"user:edit"})
    @GetMapping("/editbyid/{id}")
    public ResponseBean<UserEditView> edit(@PathVariable Long id) throws SystemException {
        return ResponseBean.success(userService.edit(id));
    }


    /**
     * 获取个人信息详情，编辑个人信息
     * @param username
     * @return
     */
    @RequiresPermissions({"userinfo:edit"})
    @GetMapping("/editbyname/{username}")
    public ResponseBean<UserView> edit(@PathVariable String username) throws SystemException {
        return ResponseBean.success(userService.edit(username));
    }


    /**
     * 添加用户
     * @param userView
     * @return
     */
    @RequiresPermissions({"user:add"})
    @PostMapping("/add")
    public ResponseBean add(@RequestBody UserView userView ) throws SystemException {
        int i = userService.add(userView);
        if (i >= 1){
            return ResponseBean.success();
        }
        return ResponseBean.error("添加用户失败");
    }

    /**
     * 根据用户id，获取用户已经拥有的角色
     * @param id
     * @return
     */
    @GetMapping("/{id}/roles")
    public ResponseBean<Map<String, Object>> roles(@PathVariable Long id) throws SystemException {
        //用户拥有的角色
        List<Long> values = userService.getRolesById(id);
        //获取系统里所有的角色并转成前端需要的角色Item
        List<RoleTransferItemView> items = roleService.findAll();
        Map<String, Object> map = new HashMap<>();
        map.put("roles", items);
        map.put("values", values);
        return ResponseBean.success(map);
    }


    /**
     * 修改密码
     * @param
     * @param password
     * @return
     */
    @PutMapping("/changepwd/{username}/{password}")
    public ResponseBean changepwd(@PathVariable String username, @PathVariable String password){
        if (userService.changepwd(username, password) >= 1){
            return ResponseBean.success();
        }
        return ResponseBean.error("密码修改失败");
    }

    /**
     *
     * @param
     * @return
     */
    @RequiresPermissions({"userinfo:editpwd"})
    @GetMapping("/checkpwd/{username}/{password}")
    public ResponseBean checkPwd(@PathVariable String username,@PathVariable String password){
        if (userService.checkPwd(username, password) == 1){
            return ResponseBean.success();
        }
        return ResponseBean.error("密码错误！！！");
    }


}
