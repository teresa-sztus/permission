package com.sztus.permission.shiro.object.converter;

import com.sztus.permission.shiro.object.domain.Department;
import com.sztus.permission.shiro.object.type.view.DepartmentView;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DepartmentConverter {

    DepartmentConverter INSTANCE = Mappers.getMapper(DepartmentConverter.class);
    List<DepartmentView> departmentListToViewList(List<Department> departments);
    DepartmentView departmentToView(Department department);

    Department departmentViewToEntity(DepartmentView departmentView);

}
