package com.sztus.permission.shiro.object.converter;

import com.sztus.permission.shiro.object.domain.Menu;
import com.sztus.permission.shiro.object.type.view.MenuNodeView;
import com.sztus.permission.shiro.object.type.view.MenuView;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface MenuConverter {

    MenuConverter INSTANCE = Mappers.getMapper(MenuConverter.class);

    MenuNodeView MenuToNodeView(Menu menu);

    List<MenuNodeView> MenuListToViewList(List<Menu> menuList);

    Menu MenuViewToEntity(MenuView menuView);

    MenuView MenuToView(Menu menu);

}
