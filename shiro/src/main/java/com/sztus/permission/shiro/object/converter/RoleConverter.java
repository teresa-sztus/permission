package com.sztus.permission.shiro.object.converter;

import com.sztus.permission.shiro.object.domain.Role;
import com.sztus.permission.shiro.object.type.view.RoleView;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface RoleConverter {

    RoleConverter INSTANCE = Mappers.getMapper(RoleConverter.class);

    RoleView RoleToView(Role role);

    List<RoleView> RoleListToView(List<Role> roleList);

    Role RoleViewToEntity(RoleView roleView);

}
