package com.sztus.permission.shiro.object.converter;

import com.sztus.permission.shiro.object.domain.User;
import com.sztus.permission.shiro.object.type.view.UserEditView;
import com.sztus.permission.shiro.object.type.view.UserView;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserConverter {

    UserConverter INSTANCE = Mappers.getMapper(UserConverter.class);

    User userViewToEntity(UserView userView);

    User UserEditViewToEntiy(UserEditView userEditView);

    UserView UserToUserView(User user);

    UserEditView UserToEditView(User user);

}
