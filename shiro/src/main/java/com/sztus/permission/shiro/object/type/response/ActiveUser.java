package com.sztus.permission.shiro.object.type.response;

import com.sztus.permission.shiro.object.domain.Menu;
import com.sztus.permission.shiro.object.domain.Role;
import com.sztus.permission.shiro.object.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActiveUser {

    /**
     * 当前用户对象
     */
    private User user;
    /**
     * 当前用户具有的角色
     */
    private List<Role> roles;
    /**
     * 当前用户具有的url
     */
    private List<String> urls;

    /**
     * 拥有的菜单权限
     */
    private List<Menu> menus;

    /**
     * 拥有的按钮
     */
    private List<String> marks;
    /**
     * session id
     */
    private String id;
    /**
     * 用户 id
     */
    private String userId;
    /**
     * 用户名称
     */
    private String username;
    /**
     * session 创建时间
     */
    private String startTimestamp;
    /**
     * session 最后访问时间
     */
    private String lastAccessTime;
    /**
     * 超时时间
     */
    private Long timeout;
    /**
     * 是否为当前登录用户
     */
    private Boolean current;


}
