package com.sztus.permission.shiro.object.type.view;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MenuNodeView {

    private Long id;

    private Long parentId;

    private String menuName;

    private Integer type;

    private String icon;

    private String  mark;

    private String url;

    private Integer open;

    //子节点
    private List<MenuNodeView> children=new ArrayList<>();


}
