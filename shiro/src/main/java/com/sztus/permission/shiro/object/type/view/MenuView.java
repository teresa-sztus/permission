package com.sztus.permission.shiro.object.type.view;

import lombok.Data;

import java.util.Date;

@Data
public class MenuView {

    private Long id;


    private Long parentId;


    private String menuName;


    private Integer type;


    private String icon;


    private String  mark;


    private String url;


    private Integer open;

    private Date createTime;


    private Date modifiedTime;

}
