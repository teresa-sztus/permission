package com.sztus.permission.shiro.object.type.view;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PageView<T> {
    private long total;

    private List<T> rows=new ArrayList<>();

    public PageView(long total, List<T> data) {
        this.total = total;
        this.rows = data;
    }

}
