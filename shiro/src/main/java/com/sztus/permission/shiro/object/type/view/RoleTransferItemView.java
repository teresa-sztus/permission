package com.sztus.permission.shiro.object.type.view;

import lombok.Data;

@Data
public class RoleTransferItemView {
    private Long key;
    private String label;
}
