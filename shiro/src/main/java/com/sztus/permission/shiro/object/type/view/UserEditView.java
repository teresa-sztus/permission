package com.sztus.permission.shiro.object.type.view;

import lombok.Data;

import java.util.Date;

@Data
public class UserEditView {

    private Long id;


    private String username;


    private Long departmentId;


    private String email;


    private String mobile;


    private Integer sex;


    private Date modifiedTime;

}
