package com.sztus.permission.shiro.object.type.view;

import lombok.Data;

import java.util.List;

@Data
public class UserInfoView {
    //用户名
    private String username;

    //菜单
    private List<String> urls;

    //邮箱
    private String email;

    //电话号码
    private String mobile;

    //性别
    private Integer sex;

    //按钮权限
    private List<String> marks;

    //角色
    private List<String> roles;

    //部门
    private String department;

    //是否为超级管理员
    private Boolean isAdmin=false;
}
