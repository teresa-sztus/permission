package com.sztus.permission.shiro.object.type.view;

import lombok.Data;

import java.util.Date;

@Data
public class UserView {

    private Long id;

    private Long departmentId;

    private String username;

    private String departmentName;

    private String password;

    private String email;

    private String mobile;

    private Integer sex;

    private Date createTime;

    private Date modifiedTime;

}
