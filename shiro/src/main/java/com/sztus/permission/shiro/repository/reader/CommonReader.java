package com.sztus.permission.shiro.repository.reader;

import com.google.common.collect.Maps;
import com.sztus.framework.component.database.constant.ConditionTypeConst;
import com.sztus.framework.component.database.core.BaseJdbcReader;
import com.sztus.framework.component.database.type.SqlOption;
import com.sztus.permission.shiro.object.domain.Department;
import com.sztus.permission.shiro.object.domain.Menu;
import com.sztus.permission.shiro.object.domain.Role;
import com.sztus.permission.shiro.object.domain.User;
import com.sztus.permission.shiro.object.type.view.UserView;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class CommonReader  extends BaseJdbcReader {

    public User getUserByName(String name) {
        SqlOption sqlOption = SqlOption.getInstance();
        sqlOption.whereFormat(ConditionTypeConst.AND, "username = '%s'", name);
        return findByOptions(User.class, sqlOption.toString());
    }


    public List<Role> findRolesByUserId(Long userId){
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT r1.*  from role r1 ");
        sqlBuilder.append(" RIGHT JOIN user_role ur1 on r1.id = ur1.role_id");
        sqlBuilder.append(" RIGHT JOIN user u1 on ur1.user_id = u1.id");
        sqlBuilder.append(" WHERE u1.id = :id");
        Map<String, Object> paramMap = Maps.newHashMapWithExpectedSize(1);
        paramMap.put("id",userId);
        try {
            return namedJdbcTemplate().query(sqlBuilder.toString(), paramMap, new BeanPropertyRowMapper<>(Role.class));
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }


    public List<Menu> findMenusByRole(List<Long> roleIdList) {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT m1.*  from menu m1");
        sqlBuilder.append(" RIGHT JOIN role_menu rm1 on m1.id = rm1.menu_id ");
        sqlBuilder.append(" RIGHT JOIN role r1 on r1.id = rm1.role_id");
        sqlBuilder.append(" where r1.id in (:id) ");
        Map<String, Object> paramMap = Maps.newHashMapWithExpectedSize(1);
        paramMap.put("id",roleIdList);
        try {
            return namedJdbcTemplate().query(sqlBuilder.toString(), paramMap, new BeanPropertyRowMapper<>(Menu.class));
        } catch (Exception e) {
            return new ArrayList<>();
        }

    }

    public List<String> findUrlById(Set<Long> menuId) {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT url FROM menu");
        sqlBuilder.append(" WHERE id in (:id)");
        sqlBuilder.append(" and type = 0");
        sqlBuilder.append(" and url != ''");
        Map<String, Object> paramMap = Maps.newHashMapWithExpectedSize(1);
        paramMap.put("id",menuId);
        try {
            return namedJdbcTemplate().query(sqlBuilder.toString(), paramMap, new SingleColumnRowMapper<>(String.class));
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<String> findMarkById(Set<Long> menuId) {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT mark FROM menu");
        sqlBuilder.append(" WHERE id in (:id)");
        sqlBuilder.append(" and type = 1");
        sqlBuilder.append(" and mark != ''");
        Map<String, Object> paramMap = Maps.newHashMapWithExpectedSize(1);
        paramMap.put("id",menuId);
        try {
            return namedJdbcTemplate().query(sqlBuilder.toString(), paramMap, new SingleColumnRowMapper<>(String.class));
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }


    public Department getDepartmentByUser(Long userId) {

        String sql = "SELECT d.* from department d " +
                "RIGHT JOIN user u on u.department_id = d.id " +
                "WHERE u.id = :id";
        HashMap<String, Object> resourceMap = new HashMap<>();
        resourceMap.put("id", userId);
        try {
            return namedJdbcTemplate().queryForObject(sql, resourceMap, new BeanPropertyRowMapper<>(Department.class));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Long> findOpenIds(){
        String sqlBuilder = "SELECT id FROM menu WHERE `open` = 1 ";
        return namedJdbcTemplate().query(sqlBuilder.toString(), new SingleColumnRowMapper<>(Long.class));
    }

    public List<Long> findMenuIdByRoleId(Long id) {
        String sqlBuilder = "SELECT menu_id FROM role_menu WHERE role_id =:id";
        HashMap<String, Object> resourceMap = new HashMap<>();
        resourceMap.put("id", id);
        return namedJdbcTemplate().query(sqlBuilder, resourceMap, new SingleColumnRowMapper<>(Long.class));
    }

    public List<UserView> findUserList(Integer pageNum, Integer pageSize, UserView userView){
        String sql ="SELECT u.id,u.username,u.sex,u.email,u.mobile,d.department_name from `user` u " +
                " JOIN department d ON d.id = u.department_id";
        Map<String, Object> paramMap = Maps.newHashMapWithExpectedSize(2);
        if (StringUtils.isNotBlank(userView.getUsername())){
            sql += " and u.username like '%userName%' ";
            sql = sql.replace("userName", userView.getUsername());
        }
        if(Objects.nonNull(userView.getSex())){
            sql += " and u.sex = :sex";
            paramMap.put("sex",userView.getSex());
        }
        if (Objects.nonNull(userView.getDepartmentId())){
            sql += " and d.id = :departmentId";
            paramMap.put("departmentId",userView.getDepartmentId());
        }
        sql += " LIMIT " + pageSize * (pageNum - 1) + ", " + pageSize;
        try {
            return namedJdbcTemplate().query(sql,paramMap, new BeanPropertyRowMapper<>(UserView.class));
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<Long> findRolesId(Long userId) {
        String sqlBuilder = "SELECT role_id FROM user_role WHERE user_id =:id";
        HashMap<String, Object> resourceMap = new HashMap<>();
        resourceMap.put("id", userId);
        return namedJdbcTemplate().query(sqlBuilder, resourceMap, new SingleColumnRowMapper<>(Long.class));
    }
}
