package com.sztus.permission.shiro.repository.writer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.sztus.framework.component.core.util.DateUtil;
import com.sztus.framework.component.core.util.StringUtil;
import com.sztus.framework.component.database.core.BaseJdbcWriter;
import com.sztus.framework.component.database.kit.SqlBuilder;
import com.sztus.permission.shiro.object.converter.RoleConverter;
import com.sztus.permission.shiro.object.converter.UserConverter;
import com.sztus.permission.shiro.object.domain.Department;
import com.sztus.permission.shiro.object.domain.Role;
import com.sztus.permission.shiro.object.domain.User;
import com.sztus.permission.shiro.object.type.view.DepartmentView;
import com.sztus.permission.shiro.object.type.view.RoleView;
import com.sztus.permission.shiro.object.type.view.UserEditView;
import com.sztus.permission.shiro.object.type.view.UserView;
import com.sztus.permission.shiro.util.MD5Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

@Repository
public class CommonWriter extends BaseJdbcWriter {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommonWriter.class);
     public Integer insertDepartment(DepartmentView departmentView){
         String date = DateUtil.dateToStr(new Date(), "yyyy-MM-dd HH:mm:ss.SSS");
         StringBuilder sql = new StringBuilder("INSERT INTO department(department_name, phone,create_time,modified_time) VALUES ");
         String val = "('" + departmentView.getDepartmentName().toString() + "','" + departmentView.getPhone().toString()  + "','" +
                 date + "','" + date +"')";
         sql.append(val);
         return jdbcTemplate().update(String.valueOf(sql));
     }

    public <E> int insertOrUpdateEntity(Class<E> entityClass, E entity) {
        int count;
        try {
            String sql = generateInsertOrUpdateSql(entityClass);
            return namedJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(entity));
        } catch (Exception e) {
            count = -1;
        }
        return count;
    }

    private <E> String generateInsertOrUpdateSql(Class<E> entityClass) {
        StringBuilder sqlBuilder = new StringBuilder();
        StringBuilder fieldsBuilder = new StringBuilder();
        StringBuilder valuesBuilder = new StringBuilder();
        StringBuilder updateBuilder = new StringBuilder();
        int rowCount = 0;
        for (Field field : entityClass.getDeclaredFields()) {
            if (rowCount > 0) {
                fieldsBuilder.append(", ");
                valuesBuilder.append(", ");
                updateBuilder.append(", ");
            }
            String field_name = StringUtil.formatCamelToUnderscore(field.getName());
            fieldsBuilder.append(String.format(" `%s` ", field_name));
            valuesBuilder.append(String.format(" :%s ", field.getName()));
            updateBuilder.append(String.format(" `%s`=VALUES(`%s`) ", field_name, field_name));
            rowCount++;
        }
        sqlBuilder.append(" INSERT INTO ");
        sqlBuilder.append(String.format(" `%s` ", StringUtil.formatCamelToUnderscore(entityClass.getSimpleName())));
        sqlBuilder.append(String.format(" (%s) ", fieldsBuilder.toString()));
        sqlBuilder.append(" VALUES ");
        sqlBuilder.append(String.format(" (%s) ", valuesBuilder.toString()));
        sqlBuilder.append(" ON DUPLICATE KEY UPDATE ");
        sqlBuilder.append(updateBuilder.toString());
        return sqlBuilder.toString();
    }


    /**
     * 批量更新
     */
    public <E> Long batchSaveList(Class<E> entityClass, Collection<E> collection) {
        return batchSave(entityClass, JSON.parseArray(JSON.toJSONString(collection, SerializerFeature.WriteMapNullValue)));

    }

    protected  <E> Long batchSave(Class<E> entityClass, JSONArray entityArray) {
        long result = 0;

        if (entityArray.size() <= 0) {
            return result;
        }

        JSONArray entityForInserting = new JSONArray();

        entityArray.forEach(entityObject -> {
            JSONObject entityJson = (JSONObject) entityObject;
            entityForInserting.add(entityJson);
        });

        if (entityForInserting.size() > 0) {
            try (SqlBuilder sqlBuilder = new SqlBuilder()) {
                sqlBuilder.insertOrUpdate(entityClass);
                entityForInserting.forEach(entityObject -> {
                    JSONObject entityJson = (JSONObject) entityObject;
                    sqlBuilder.values(entityJson);
                });

                int[] affectedArray = namedJdbcTemplate().batchUpdate(
                        sqlBuilder.getSqlStatement(),
                        sqlBuilder.getSqlParameterSourceArray()
                );
                result += affectedArray.length;
            } catch (Exception e) {
                result = 0;
            }
        }
        return result;
    }

    public int addUser(UserView userView){
        User user = UserConverter.INSTANCE.userViewToEntity(userView);
        java.sql.Date date = new java.sql.Date(new Date().getTime());
        user.setCreateTime(date);
        user.setModifiedTime(date);
        //默认为普通用户
        user.setType(1);
        String salt= UUID.randomUUID().toString().substring(0,32);
        user.setPassword(MD5Utils.md5Encryption(user.getPassword(), salt));
        user.setSalt(salt);
//        if(i<1){
//            throw new SystemException(CommonErrorCode.ERROR_CODE,"添加用户失败");
//        }
        return this.insertOrUpdateEntity(User.class, user);
    }



    public int updatePwd(String username, String pwd, User user) {
        StringBuilder sql = new StringBuilder("UPDATE user SET ");
        sql.append(String.format("password =  '%s'  WHERE username = '%s' ",pwd,username));
        return namedJdbcTemplate().update(String.valueOf(sql), new BeanPropertySqlParameterSource(user));
    }

    public int updateUser(Long id, UserEditView userEditView) {
        //更新
        User user = UserConverter.INSTANCE.UserEditViewToEntiy(userEditView);
        StringBuilder sql = new StringBuilder("UPDATE user SET ");
        sql.append(String.format("email =  '%s' , mobile = '%s' , modified_time = '%s' WHERE username = '%s' ",
                userEditView.getEmail(),
                userEditView.getMobile(),
                new java.sql.Date(new Date().getTime()),
                userEditView.getUsername()));
        return namedJdbcTemplate().update(String.valueOf(sql), new BeanPropertySqlParameterSource(user));
    }

    public int updateDepartment(Long id, Department department) {
        StringBuilder sql = new StringBuilder("UPDATE department SET ");
        sql.append(String.format("department_name =  '%s' , phone = '%s' , modified_time = '%s' WHERE id = '%s' ",
                department.getDepartmentName(),
                department.getPhone(),
                new java.sql.Date(new Date().getTime()),
                id));
        return namedJdbcTemplate().update(String.valueOf(sql), new BeanPropertySqlParameterSource(department));
    }

    public int updateRole(Long id, RoleView roleView) {
        Role newRole = RoleConverter.INSTANCE.RoleViewToEntity(roleView);
        StringBuilder sql = new StringBuilder("UPDATE role SET ");
        sql.append(String.format("role_name =  '%s' , remark = '%s' , modified_time = '%s' WHERE id = '%s' ",
                roleView.getRoleName(),
                roleView.getRemark(),
                new java.sql.Date(new Date().getTime()),
                id));
        return namedJdbcTemplate().update(String.valueOf(sql), new BeanPropertySqlParameterSource(newRole));
    }
}
