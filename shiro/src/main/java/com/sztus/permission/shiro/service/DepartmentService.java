package com.sztus.permission.shiro.service;

import com.sztus.framework.component.database.type.SqlOption;
import com.sztus.permission.shiro.component.handler.SystemException;
import com.sztus.permission.shiro.object.converter.DepartmentConverter;
import com.sztus.permission.shiro.object.domain.Department;
import com.sztus.permission.shiro.object.enumerate.CommonErrorCode;
import com.sztus.permission.shiro.object.type.view.DepartmentView;
import com.sztus.permission.shiro.object.type.view.PageView;
import com.sztus.permission.shiro.repository.reader.CommonReader;
import com.sztus.permission.shiro.repository.writer.CommonWriter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.List;
import java.util.Objects;
@Service
public class DepartmentService {

    private final CommonReader commonReader;

    private final CommonWriter commonWriter;

    public DepartmentService(CommonReader commonReader, CommonWriter commonWriter) {
        this.commonReader = commonReader;
        this.commonWriter = commonWriter;
    }

    public DepartmentView edit(Department department) throws SystemException {
        if(Objects.isNull(department)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"编辑的部门不存在");
        }
        return DepartmentConverter.INSTANCE.departmentToView(department);
    }

    public DepartmentView edit(Long id) throws SystemException {
        Department department = commonReader.findById(Department.class, id, null);
        return this.edit(department);
    }

    public PageView<DepartmentView> findDepartmentList(Integer pageNum, Integer pageSize, DepartmentView departmentView) {


        SqlOption option = SqlOption.getInstance();
        if(Objects.nonNull(departmentView.getDepartmentName())){
            option.whereLike("department_name",departmentView.getDepartmentName(),null);
        }
        option.page(pageNum,pageSize);
        List<Department> departments = commonReader.findAllByOptions(Department.class, option.toString());
         return new PageView<>(departments.size(),DepartmentConverter.INSTANCE.departmentListToViewList(departments));
    }


    public List<DepartmentView> findAll() {
        List<Department> departments = commonReader.findAllByOptions(Department.class, null);
        return DepartmentConverter.INSTANCE.departmentListToViewList(departments);
    }

    /**
     * 添加部门
     * @param departmentView
     */
    @Transactional
    public void add(DepartmentView departmentView) throws SystemException {
        //判断修改后的部门名是否已经存在
        String name = departmentView.getDepartmentName();
        SqlOption sqlOption = SqlOption.getInstance();
        sqlOption.whereEqual("department_name",name);
        Department department = commonReader.findByOptions(Department.class, sqlOption.toString());
        if (Objects.nonNull(department)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"该部门已经存在");
        }
        //添加部门
        Integer i = commonWriter.insertDepartment(departmentView);
//        //添加部门菜单
//        String departmentName = departmentView.getDepartmentName();
//        String menuMark = departmentView.getMenuMark();
//        Menu menu = new Menu();
//        java.sql.Date date = new java.sql.Date(new Date().getTime());
//        menu.setCreateTime(date);
//        menu.setModifiedTime(date);
//        menu.setMenuName(departmentName);
//        menu.setIcon("el-icon-monitor");
//        menu.setUrl("/business/"+menuMark);
//        menu.setType(0);
//        menu.setParentId(2L);
//        menu.setOpen(0);
//        int i2 = commonWriter.insertOrUpdateEntity(Menu.class, menu);
        if (i<1){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"添加部门失败");
        }

    }

    public int update(Long id, DepartmentView departmentView) throws SystemException, ParseException {
        Department department = commonReader.findById(Department.class, id, null);
        if(Objects.isNull(department)){
            throw  new SystemException(CommonErrorCode.ERROR_CODE,"要更新的部门不存在");
        }
        //判断修改后的部门名是否已经存在
        String name = departmentView.getDepartmentName();
        SqlOption sqlOption = SqlOption.getInstance();
        sqlOption.whereEqual("department_name",name);
        Department department2 = commonReader.findByOptions(Department.class, sqlOption.toString());
        if (Objects.nonNull(department2) && !department2.getDepartmentName().equals(name)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"该部门已经存在");
        }
        Department department1 = DepartmentConverter.INSTANCE.departmentViewToEntity(departmentView);
        return commonWriter.updateDepartment(id, department1);
    }

    public void delete(Long id) throws SystemException {
        Department department = commonReader.findById(Department.class, id, null);
        if(Objects.isNull(department)){
            throw  new SystemException(CommonErrorCode.ERROR_CODE,"要删除的部门不存在");
        }
        SqlOption option = SqlOption.getInstance();
        option.whereEqual("id", id);
        Long l = commonWriter.deleteByOptions(Department.class, option.toString());
        if (l < 1){
            throw  new SystemException(CommonErrorCode.ERROR_CODE,"删除部门失败");
        }
    }
}
