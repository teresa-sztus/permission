package com.sztus.permission.shiro.service;

import com.alibaba.nacos.common.utils.CollectionUtils;
import com.sztus.permission.shiro.component.handler.SystemException;
import com.sztus.permission.shiro.config.JWTToken;
import com.sztus.permission.shiro.object.converter.MenuConverter;
import com.sztus.permission.shiro.object.domain.Department;
import com.sztus.permission.shiro.object.domain.Menu;
import com.sztus.permission.shiro.object.domain.Role;
import com.sztus.permission.shiro.object.domain.User;
import com.sztus.permission.shiro.object.enumerate.CommonErrorCode;
import com.sztus.permission.shiro.object.enumerate.UserTypeEnum;
import com.sztus.permission.shiro.object.type.response.ActiveUser;
import com.sztus.permission.shiro.object.type.view.DepartmentView;
import com.sztus.permission.shiro.object.type.view.MenuNodeView;
import com.sztus.permission.shiro.object.type.view.UserInfoView;
import com.sztus.permission.shiro.repository.reader.CommonReader;
import com.sztus.permission.shiro.util.JWTUtils;
import com.sztus.permission.shiro.util.MD5Utils;
import com.sztus.permission.shiro.util.MenuTreeBuilder;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class LoginService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginService.class);

    private final CommonReader commonReader;

    private final DepartmentService departmentService;

    public LoginService(CommonReader commonReader, DepartmentService departmentService) {
        this.commonReader = commonReader;
        this.departmentService = departmentService;
    }

    public User findUserByName(String name){
        return  commonReader.getUserByName(name);
    }

    public String login(String username,String password) throws SystemException {
        String token;
        User user = findUserByName(username);
        if(!Objects.isNull(user)){
            String salt = user.getSalt();
            //加密
            String target = MD5Utils.md5Encryption(password,salt);
            //生成token
            token = JWTUtils.sign(username, target);
            JWTToken jwtToken = new JWTToken(token);
            try{
                SecurityUtils.getSubject().login(jwtToken);
            }catch (AuthenticationException e){
               throw new SystemException(CommonErrorCode.ERROR_CODE,e.getMessage());
            }
        }else {
            throw new SystemException(CommonErrorCode.ERROR_CODE,"用户不存在");
        }
        return token;
    }

    /**
     * 查询用户角色
     * @param id
     * @return
     * @throws SystemException
     */
    public List<Role> findRolesById(Long id) throws SystemException {

        User user = commonReader.findById(User.class, id, null);
        if(Objects.isNull(user)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"该用户不存在");
        }
        //通过userid查找用户拥有的角色
        return commonReader.findRolesByUserId(id);

    }


    /**
     * 查询用户所拥有的菜单权限
     * @param roles
     * @return
     */
    public List<Menu> findMenuByRole(List<Role> roles) {
        List<Long> roleIdList= new ArrayList<>();
        if(CollectionUtils.isNotEmpty(roles)){
            for (Role role : roles){
                roleIdList.add(role.getId());
            }
        }
        return commonReader.findMenusByRole(roleIdList);
    }

    public List<String> findUrlById(Set<Long> menuId) {
         return commonReader.findUrlById(menuId);
    }

    public List<String> findMarkById(Set<Long> menuId) {
        return commonReader.findMarkById(menuId);
    }


    /**
     * 获取用户相关信息
     */
    public UserInfoView getUserInfo() throws SystemException {
        ActiveUser activeUser = (ActiveUser) SecurityUtils.getSubject().getPrincipal();
        UserInfoView userInfoView = new UserInfoView();
        //个人信息
        userInfoView.setUsername(activeUser.getUser().getUsername());
        userInfoView.setEmail(activeUser.getUser().getEmail());
        userInfoView.setMobile(activeUser.getUser().getMobile());
        userInfoView.setSex(activeUser.getUser().getSex());

        userInfoView.setUrls(activeUser.getUrls());
        List<String> roleNames = activeUser.getRoles().stream().map(Role::getRoleName).collect(Collectors.toList());
        userInfoView.setRoles(roleNames);
        userInfoView.setMarks(activeUser.getMarks());
        userInfoView.setIsAdmin(activeUser.getUser().getType().equals(UserTypeEnum.SYSTEM_ADMIN.getTypeCode()));

        Department department = commonReader.getDepartmentByUser(activeUser.getUser().getId());

        DepartmentView departmentView = departmentService.edit(department);
        if(!Objects.isNull(departmentView)){
            userInfoView.setDepartment(departmentView.getDepartmentName());
        }
        return userInfoView;
    }

    public List<MenuNodeView> findMenu() {
        List<Menu> menus=null;
        List<MenuNodeView> menuNodeViews = new ArrayList<>();

        ActiveUser activeUser = (ActiveUser) SecurityUtils.getSubject().getPrincipal();
        if(activeUser.getUser().getType().equals(UserTypeEnum.SYSTEM_ADMIN.getTypeCode())){
            //超级管理员
            menus=commonReader.findAllByOptions(Menu.class,null);

        }else if(activeUser.getUser().getType().equals(UserTypeEnum.SYSTEM_USER.getTypeCode())){
            //普通系统用户
            menus= activeUser.getMenus();
        }
        if(!CollectionUtils.isEmpty(menus)){
            for (Menu menu : menus){
                //type=0的是菜单，1为按钮
                if (menu.getType().equals(0)){
                    MenuNodeView menuNodeView = MenuConverter.INSTANCE.MenuToNodeView(menu);
                    menuNodeViews.add(menuNodeView);
                }
            }
        }
        //构建树形菜单
        return MenuTreeBuilder.build(menuNodeViews);

    }

}
