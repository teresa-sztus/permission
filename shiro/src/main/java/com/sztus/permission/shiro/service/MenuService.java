package com.sztus.permission.shiro.service;

import com.sztus.framework.component.database.type.SqlOption;
import com.sztus.permission.shiro.component.handler.SystemException;
import com.sztus.permission.shiro.object.converter.MenuConverter;
import com.sztus.permission.shiro.object.domain.Menu;
import com.sztus.permission.shiro.object.enumerate.CommonErrorCode;
import com.sztus.permission.shiro.object.type.view.MenuNodeView;
import com.sztus.permission.shiro.object.type.view.MenuView;
import com.sztus.permission.shiro.repository.reader.CommonReader;
import com.sztus.permission.shiro.repository.writer.CommonWriter;
import com.sztus.permission.shiro.util.MenuTreeBuilder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MenuService {

    private final CommonReader commonReader;
    private final CommonWriter commonWriter;


    public MenuService(CommonReader commonReader, CommonWriter commonWriter) {
        this.commonReader = commonReader;
        this.commonWriter = commonWriter;
    }

    public void deleteMenuById(Long id) throws SystemException {
        SqlOption option = SqlOption.getInstance();
        option.whereEqual("id",id);
        Long aLong = commonWriter.deleteByOptions(Menu.class, option.toString());
        if (aLong < 1){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"删除菜单失败");
        }

    }

    public List<MenuNodeView> findMenuTree() {
        SqlOption sqlOption = SqlOption.getInstance();
        sqlOption.order(" parent_id ASC");
        List<Menu> menuList = commonReader.findAllByOptions(Menu.class, sqlOption.toString());
        List<MenuNodeView> menuNodeViews = MenuConverter.INSTANCE.MenuListToViewList(menuList);
        return MenuTreeBuilder.build(menuNodeViews);
    }

    public List<Long> findOpenIds() {
        List<Long> openIds = commonReader.findOpenIds();
        return openIds;
    }

    public Menu add(MenuView menuView) throws SystemException {
        java.sql.Date date = new java.sql.Date(new Date().getTime());
        menuView.setCreateTime(date);
        menuView.setModifiedTime(date);
        Menu menu = MenuConverter.INSTANCE.MenuViewToEntity(menuView);
        int i = commonWriter.insertOrUpdateEntity(Menu.class, menu);
        if (i >= 1){
            return menu;
        }
        throw  new SystemException(CommonErrorCode.ERROR_CODE,"添加菜单失败");
    }

    public MenuView edit(Long id) {
        Menu menu = commonReader.findById(Menu.class, id, null);
        return MenuConverter.INSTANCE.MenuToView(menu);
    }

    public void update(Long id, MenuView menuView) throws SystemException {
        Menu menu = MenuConverter.INSTANCE.MenuViewToEntity(menuView);
        int i = commonWriter.insertOrUpdateEntity(Menu.class, menu);
        if (i < 1){
            throw  new SystemException(CommonErrorCode.ERROR_CODE,"更新菜单失败");
        }
    }
}
