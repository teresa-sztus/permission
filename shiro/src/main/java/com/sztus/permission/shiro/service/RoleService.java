package com.sztus.permission.shiro.service;

import com.sztus.framework.component.database.constant.ConditionTypeConst;
import com.sztus.framework.component.database.type.SqlOption;
import com.sztus.permission.shiro.component.handler.SystemException;
import com.sztus.permission.shiro.object.converter.RoleConverter;
import com.sztus.permission.shiro.object.domain.Role;
import com.sztus.permission.shiro.object.domain.RoleMenu;
import com.sztus.permission.shiro.object.enumerate.CommonErrorCode;
import com.sztus.permission.shiro.object.type.view.PageView;
import com.sztus.permission.shiro.object.type.view.RoleTransferItemView;
import com.sztus.permission.shiro.object.type.view.RoleView;
import com.sztus.permission.shiro.repository.reader.CommonReader;
import com.sztus.permission.shiro.repository.writer.CommonWriter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class RoleService {

    private final CommonReader commonReader;

    private final CommonWriter commonWriter;

    public RoleService(CommonReader commonReader, CommonWriter commonWriter) {
        this.commonReader = commonReader;
        this.commonWriter = commonWriter;
    }


    public PageView<RoleView> findRoleList(Integer pageNum, Integer pageSize, RoleView roleView) throws SystemException {

        SqlOption option = SqlOption.getInstance();
        if(Objects.nonNull(roleView.getRoleName())){
            option.whereLike("role_name",roleView.getRoleName(),null);
        }
        option.page(pageNum,pageSize);
        List<Role> roleList = commonReader.findAllByOptions(Role.class, option.toString());
        if (roleList.size() == 0){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"角色列表为空");
        }
        return new PageView<>(roleList.size(), RoleConverter.INSTANCE.RoleListToView(roleList));

    }

    @Transactional
    public void authority(Long id, Long[] mids) throws SystemException {
        Role role = commonReader.findById(Role.class, id, null);
        if (Objects.isNull(role)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"该角色不存在");
        }
        if(Objects.isNull(mids)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"要授权的菜单为空");
        }
        //删除原来有的权限
        SqlOption option = SqlOption.getInstance();
        option.whereFormat(ConditionTypeConst.AND, "role_id = '%d'", id);
        commonWriter.deleteByOptions(RoleMenu.class,option.toString());
        //添加现在分配的权限
        List<RoleMenu> roleMenus = new ArrayList<>();
        for (Long mid : mids){
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setRole_id(id);
            roleMenu.setMenu_id(mid);
            roleMenus.add(roleMenu);
        }
        Long aLong = commonWriter.batchSaveList(RoleMenu.class, roleMenus);
        if (aLong < 1){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"角色授权失败");
        }

    }

    public List<Long> findMenuIdByRoleId(Long id) {
        List<Long> mids = commonReader.findMenuIdByRoleId(id);
        return mids;
    }


    public void add(RoleView roleView) throws SystemException {
        SqlOption sqlOption = SqlOption.getInstance();
        sqlOption.whereEqual("role_name", roleView.getRoleName());
        Role r = commonReader.findByOptions(Role.class, sqlOption.toString());
        if(Objects.nonNull(r)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"该角色名已被占用");
        }
        Role role = RoleConverter.INSTANCE.RoleViewToEntity(roleView);
        java.sql.Date date = new java.sql.Date(new Date().getTime());
        role.setCreateTime(date);
        role.setModifiedTime(date);
        int i = commonWriter.insertOrUpdateEntity(Role.class, role);
        if (i <1){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"添加角色失败");
        }

    }

    public void delete(Long id) throws SystemException {
        Role role = commonReader.findById(Role.class, id, null);
        if(Objects.isNull(role)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"要删除的角色不存在");
        }
        SqlOption option = SqlOption.getInstance();
        option.whereFormat(ConditionTypeConst.AND, "id = '%d'", id);
        Long aLong = commonWriter.deleteByOptions(Role.class, option.toString());
        if (aLong < 1){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"删除角色失败");

        }
    }

    public RoleView edit(Long id) throws SystemException {
        Role role = commonReader.findById(Role.class, id, null);
        if (Objects.isNull(role)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"要编辑的角色不存在");
        }
        return RoleConverter.INSTANCE.RoleToView(role);
    }

    public int update(Long id, RoleView roleView) throws SystemException {
        SqlOption sqlOption = SqlOption.getInstance();
        sqlOption.whereEqual("role_name", roleView.getRoleName());
        Role role = commonReader.findByOptions(Role.class, sqlOption.toString());
        if (Objects.nonNull(role) && !roleView.getRoleName().equals(role.getRoleName())){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"该角色名已被占用");
        }
        return commonWriter.updateRole(id, roleView);
    }

    public List<RoleTransferItemView> findAll() throws SystemException {
        List<Role> roles = commonReader.findAllByOptions(Role.class, null);
        if (Objects.isNull(roles)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"系统角色信息为空");
        }
        List<RoleTransferItemView> itemVOList=new ArrayList<>();
        for (Role role :roles){
            RoleTransferItemView itemView = new RoleTransferItemView();
            itemView.setKey(role.getId());
            itemView.setLabel(role.getRoleName());
            itemVOList.add(itemView);
        }
        return itemVOList;
    }
}
