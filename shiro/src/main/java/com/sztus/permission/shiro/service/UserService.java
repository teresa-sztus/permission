package com.sztus.permission.shiro.service;

import com.sztus.framework.component.database.type.SqlOption;
import com.sztus.permission.shiro.component.handler.SystemException;
import com.sztus.permission.shiro.object.converter.UserConverter;
import com.sztus.permission.shiro.object.domain.User;
import com.sztus.permission.shiro.object.domain.UserRole;
import com.sztus.permission.shiro.object.enumerate.CommonErrorCode;
import com.sztus.permission.shiro.object.type.response.ActiveUser;
import com.sztus.permission.shiro.object.type.view.UserEditView;
import com.sztus.permission.shiro.object.type.view.UserView;
import com.sztus.permission.shiro.repository.reader.CommonReader;
import com.sztus.permission.shiro.repository.writer.CommonWriter;
import com.sztus.permission.shiro.util.MD5Utils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private final CommonReader commonReader;
    private final CommonWriter commonWriter;

    public UserService(CommonReader commonReader, CommonWriter commonWriter) {
        this.commonReader = commonReader;
        this.commonWriter = commonWriter;
    }

    public List<UserView> findUserList(Integer pageNum, Integer pageSize, UserView userView) throws SystemException {
        List<UserView> userList = commonReader.findUserList(pageNum, pageSize, userView);
        if (Objects.isNull(userList)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"用户列表为空");
        }
        return userList;
    }

    public int add(UserView userView) throws SystemException {
        if(Objects.nonNull(commonReader.getUserByName(userView.getUsername()))){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"该用户名已被占用");
        }
        return commonWriter.addUser(userView);
    }

    public List<Long> getRolesById(Long userId) throws SystemException {

        List<Long> rolesId = commonReader.findRolesId(userId);
        if (Objects.isNull(rolesId)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"该用户还没有分配角色");
        }
        return rolesId;
    }

    @Transactional
    public void assignRoles(Long userId, Long[] roleIds) throws SystemException {
        if (Objects.isNull(commonReader.findById(User.class,userId,null))){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"用户不存在");
        }
        //删除用户以前拥有的角色
        SqlOption option = SqlOption.getInstance();
        option.whereEqual("user_id",userId);
        commonWriter.deleteByOptions(UserRole.class, option.toString());
        //增加现在要分配的角色
        List<UserRole> roles = new ArrayList<>();
        for (Long rid : roleIds){
            UserRole userRole = new UserRole();
            userRole.setUserId(userId);
            userRole.setRoleId(rid);
            roles.add(userRole);
        }
        Long i = commonWriter.batchSaveList(UserRole.class, roles);
        if (i < 1){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"分配角色失败");
        }
    }

    @Transactional
    public void deleteById(Long id) throws SystemException {
        if(Objects.isNull(commonReader.findById(User.class,id,null))){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"要删除的角色不存在");
        }
        //判断删除的角色是否为当前登录的用户
        ActiveUser activeUser = (ActiveUser) SecurityUtils.getSubject().getPrincipal();
        if (id.equals(activeUser.getUser().getId())){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"不能删除当前登录的用户");
        }
        //删除用户
        SqlOption sqlOption = SqlOption.getInstance();
        sqlOption.whereEqual("id",id);
        commonWriter.deleteByOptions(User.class,sqlOption.toString());
        //删除【用户-角色】中的信息
        SqlOption sqlOption1 = SqlOption.getInstance();
        sqlOption1.whereEqual("user_id",id);
        commonWriter.deleteByOptions(UserRole.class,sqlOption1.toString());
    }

    public void update(Long id, UserEditView userEditView) throws SystemException {
        if(Objects.isNull(commonReader.findById(User.class,id,null))){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"要更新的用户不存在");
        }

        int i = commonWriter.updateUser(id, userEditView);
        if (i < 1){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"更新用户失败");
        }
    }

    public UserEditView edit(Long id) throws SystemException {
        User user = commonReader.findById(User.class, id, null);
        if (Objects.isNull(user)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"获取用户信息失败");
        }
        return UserConverter.INSTANCE.UserToEditView(user);
    }

    public UserView edit(String usermane) throws SystemException {
        User user = commonReader.getUserByName(usermane);
        if (Objects.isNull(user)){
            throw new SystemException(CommonErrorCode.ERROR_CODE,"获取用户信息失败");
        }
        return UserConverter.INSTANCE.UserToUserView(user);
    }



    public int changepwd(String username, String newpwd) {
        User user = commonReader.getUserByName(username);
        //加密
        String pwd = MD5Utils.md5Encryption(newpwd,user.getSalt());
        return commonWriter.updatePwd(username, pwd, user);
    }

    public int checkPwd(String username, String password) {
        User user = commonReader.getUserByName(username);
        String salt = user.getSalt();
        //加密
        String pwd = MD5Utils.md5Encryption(password,salt);
        if (pwd.equals(user.getPassword())){
            return 1;
        }

        return 0;
    }
}

