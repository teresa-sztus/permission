package com.sztus.permission.shiro.util;

import com.sztus.permission.shiro.object.type.view.MenuNodeView;

import java.util.ArrayList;
import java.util.List;

/**
 * 构建树形菜单
 */
public class MenuTreeBuilder {

    /**
     * 构建多级菜单树
     * @param nodes
     * @return
     */
    public static List<MenuNodeView> build(List<MenuNodeView> nodes){
        //根节点
        List<MenuNodeView> rootMenu = new ArrayList<>();
        for (MenuNodeView nav : nodes) {
            //ParentId为0的是根节点
            if(nav.getParentId()==0){
                rootMenu.add(nav);
            }
        }

        //为根菜单设置子菜单
        for (MenuNodeView nav : rootMenu) {
            //获取根节点下的所有子菜单
            List<MenuNodeView> childList = getChild(nav.getId(), nodes);
            nav.setChildren(childList);//给根节点设置子节点
        }
        return rootMenu;
    }

    /**
     * 获取子菜单
     * @param id
     * @param nodes
     * @return
     */
    private static List<MenuNodeView> getChild(Long id, List<MenuNodeView> nodes) {
        //子菜单
        List<MenuNodeView> childList = new ArrayList<MenuNodeView>();
        for (MenuNodeView nav : nodes) {
            // 遍历所有节点，将所有菜单的父id与传过来的根节点的id比较
            //如果相等，说明为该根节点的子节点
            if(nav.getParentId().equals(id)){
                childList.add(nav);
            }
        }
        //递归
        for (MenuNodeView nav : childList) {
            nav.setChildren(getChild(nav.getId(), nodes));
        }

        //如果节点下没有子节点，返回一个空List（递归退出）
        if(childList.size() == 0){
            return new ArrayList<MenuNodeView>();
        }
        return childList;
    }


}
